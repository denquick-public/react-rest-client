import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux';

import store from './store';
import App from './containers/app';
import MainPanel from './containers/panels/main';
import styles from './styles/index.css'

import StoreService from './libs/store-service';

StoreService.store = store;

injectTapEventPlugin();

ReactDOM.render(
  <MuiThemeProvider>
    <Provider store={store}>
      <div>
        <App />
        <MainPanel />
      </div>
    </Provider>
  </MuiThemeProvider>
  ,
  document.getElementById('root')
);
