import { PUSH, REMOVE, REQUEST, SUCCESS } from '../constants/account';

const defaultState = {
  loading: false,
}

export default function(state = defaultState, action) {

  if (action.type === PUSH) {
    return Object.assign(state, action.payload);
  }
  if (action.type === REMOVE) {
    return defaultState;
  }
  if (action.type === REQUEST) {
    return {loading: true};
  }
  if (action.type === SUCCESS) {
    return Object.assign({loading: false}, action.payload);
  }

  return state;
}
