import { READ, REQUEST, SUCCESS, TO_EMPTY } from '../constants/campaign-edit';

const defaultState = {
  loading: false,
  entity: {},
};

export default function(state = defaultState, action) {

  if (action.type === READ) {
    const entity = action.payload;
    return Object.assign({}, defaultState, {entity});
  }
  if (action.type === REQUEST) {
    return Object.assign({ loading: true }, state);
  }
  if (action.type === SUCCESS) {
    return Object.assign({ loading: false }, state);
  }
  if (action.type === TO_EMPTY) {
    return Object.assign({}, defaultState);
  }

  return state;
}
