import { OPEN, CLOSE } from '../constants/campaign-modal';

const defaultState = {
  open: false,
};

export default function(state = defaultState, action) {
  const open = action.type === OPEN;
  return { open };
}
