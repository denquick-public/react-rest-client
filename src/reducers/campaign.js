import { CREATE, DELETE, UPDATE, READ, REQUEST, SUCCESS , SETUP_FILTER } from '../constants/campaign';

const defaultState = {
  loading: false,
  list: [],
  filter: 'all'
}

export default function(state = defaultState, action) {

  if (action.type === CREATE) {
    return Object.assign(defaultState, state);
  }
  if (action.type === DELETE) {
    return Object.assign(defaultState, state);
  }
  if (action.type === UPDATE) {
    return Object.assign({}, defaultState, state);
  }
  if (action.type === READ) {
    const list = action.payload;
    return Object.assign(state, { list });
  }
  if (action.type === REQUEST) {
    return Object.assign({}, {loading: true}, state);
  }
  if (action.type === SUCCESS) {
    return Object.assign({}, { loading: false }, state);
  }
  if (action.type === SETUP_FILTER) {
    return Object.assign({}, state, { filter: action.payload });
  }

  return state;
}
