import { OPEN, CLOSE } from '../constants/auth-modal';

const defaultState = {
  open: false,
  loginTab: true,
};

export default function(state = defaultState, action) {
  const open = action.type === OPEN;
  const loginTab = action.loginTab;
  if (open || action.type === CLOSE) {
    return { open, loginTab };
  }
  return state;
}
