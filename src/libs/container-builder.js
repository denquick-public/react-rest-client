import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as accountActions from '../actions/account';
import * as campaignActions from '../actions/campaign';
import * as campaignEditActions from '../actions/campaign-edit';
import * as authModalActions from '../actions/auth-modal';
import * as campaignModalActions from '../actions/campaign-modal';

const mapStateToProps = state => ({
  account: state.account,
  campaign: state.campaign,
  campaignEdit: state.campaignEdit,
  authModal: state.authModal,
  campaignModal: state.campaignModal,
});

const mapDispatchToProps = dispatch => ({
  authModalActions: bindActionCreators(authModalActions, dispatch),
  campaignModalActions: bindActionCreators(campaignModalActions, dispatch),
  accountActions: bindActionCreators(accountActions, dispatch),
  campaignActions: bindActionCreators(campaignActions, dispatch),
  campaignEditActions: bindActionCreators(campaignEditActions, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
);
