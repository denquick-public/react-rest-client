export const REQUIRED = 'this field is required';
export const EMAIL_IS_NOT_VALID = 'email is not valid';
export const CONFIRM_PASSWORD = 'passwords should be identity';

export default {
  REQUIRED,
  EMAIL_IS_NOT_VALID,
  CONFIRM_PASSWORD,
}
