export const PUSH   = 'ACCOUNT_PUSH';
export const REMOVE = 'ACCOUNT_REMOVE';
export const REQUEST = 'ACCOUNT_REQUEST';
export const SUCCESS = 'ACCOUNT_SUCCESS';
