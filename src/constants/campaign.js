export const CREATE       = 'CAMPAIGN_CREATE';
export const DELETE       = 'CAMPAIGN_DELETE';
export const UPDATE       = 'CAMPAIGN_UPDATE';
export const READ         = 'CAMPAIGN_READ';

export const REQUEST      = 'CAMPAIGN_REQUEST';
export const SUCCESS      = 'CAMPAIGN_SUCCESS';
export const SETUP_FILTER = 'CAMPAIGN_SETUP_FILTER';
