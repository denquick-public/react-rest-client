import { OPEN, CLOSE } from '../constants/auth-modal';

export const open = (payline)=> {
  return Object.assign({}, {
    type: OPEN,
  }, payline);
}

export const close = ()=> {
  return {
    type: CLOSE,
  }
}
