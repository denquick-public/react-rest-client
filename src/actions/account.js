import { PUSH, REMOVE, REQUEST, SUCCESS } from '../constants/account';
import { getController } from '../api/emitter';

export const add = (payload)=> {
  return {
    type: PUSH,
    payload,
  }
}

export const remove = ()=> {
  return {
    type: REMOVE,
  }
}

export const login = (payload, callback)=> {
  return (dispatch) => {
    dispatch({ type: REQUEST });

    const controller = getController('account');
    controller.login(payload, {
      dispatch, callback
    });
  };
}

export const join = (payload, callback)=> {
  return (dispatch) => {
    dispatch({ type: REQUEST });

    const controller = getController('account');
    controller.join(payload, {
      dispatch, callback
    });
  };
}
