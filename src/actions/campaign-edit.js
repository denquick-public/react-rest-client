import { READ, REQUEST, SUCCESS, TO_EMPTY } from '../constants/campaign-edit';
import { getController } from '../api/emitter';

export const read = (payload, callback)=> {
  return (dispatch) => {
    dispatch({ type: REQUEST });

    const controller = getController('campaign');
    controller.read(payload, {
      dispatch, callback
    })
    .then((reponse)=>{
      dispatch({ type: SUCCESS });
    });
  };
};

export const toEmpty = () => {
  return {
    type: TO_EMPTY
  }
};
