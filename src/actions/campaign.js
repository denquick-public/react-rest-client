import { CREATE, DELETE, UPDATE, READ, REQUEST, SUCCESS, SETUP_FILTER } from '../constants/campaign';
import { getController } from '../api/emitter';

export const create = (payload, callback)=> {
  return (dispatch) => {
    dispatch({ type: REQUEST });

    const controller = getController('campaign');
    controller.create(payload, {
      dispatch, callback
    })
    .then((reponse)=>{
      dispatch({ type: SUCCESS });
    });
  };
}

export const remove = (payload, callback)=> {
  return (dispatch) => {
    dispatch({ type: REQUEST });

    const controller = getController('campaign');
    controller.delete(payload, {
      dispatch, callback
    })
    .then((reponse)=>{
      dispatch({ type: SUCCESS });
    });
  };
}

export const update = (payload, callback)=> {
  return (dispatch) => {
    dispatch({ type: REQUEST });

    const controller = getController('campaign');
    controller.update(payload, {
      dispatch, callback
    })
    .then((reponse)=>{
      dispatch({ type: SUCCESS });
    });
  };
}

export const read = (payload, callback)=> {
  return (dispatch) => {
    dispatch({ type: REQUEST });

    const controller = getController('campaign');
    controller.read(payload, {
      dispatch, callback
    })
    .then((reponse)=>{
      dispatch({ type: SUCCESS });
    });
  };
}

export const setupFilter = (payload) => {
  return {
    type: SETUP_FILTER,
    payload,
  }
}
