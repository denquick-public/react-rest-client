import { OPEN, CLOSE } from '../constants/campaign-modal';

export const open = (payline)=> {
  return Object.assign({}, {
    type: OPEN,
  }, payline);
}

export const close = ()=> {
  return {
    type: CLOSE,
  }
}
