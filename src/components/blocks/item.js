import React, { Component } from 'react';
import * as colors from 'material-ui/styles/colors';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';

import Delete from 'material-ui/svg-icons/action/delete';
import Update from 'material-ui/svg-icons/action/update';

const styles = {
  block: {
    height: 150,
    width: 150,
    margin: 20,
    textAlign: 'center',
    display: 'inline-block',
  },
  menu: {
    position: 'relative',
    top: 96,
  }
};

export default class ItemBlock extends Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  handleTouchTap = (event) => {
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  }

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  }

  handleUpdate = () => {
    const campaignId = this.props.data._id;
    this.props.campaignEditActions.read({
      campaignId,
    }, ()=>{});
  }

  handleRemove = () => {
    const campaignId = this.props.data._id;
    const { remove, read } = this.props.campaignActions;
      remove({campaignId}, ()=> {
      const params = { userId: this.props.account._id };
      read({params}, ()=> {});
    });
  }

  handleChange = (value)=>{
    const data = this.props.data;
    const campaignId = data._id;

    const payload = {
      campaignId,
      type: value,
      title: data.title,
      userId: data.userId,
    };

    this.props.campaignActions.update(payload, ()=> {
      const params  = {
        type: this.props.campaign.filter,
      };
      this.props.campaignActions.read({params}, ()=> {});
    });
  }

  render() {
    const data = this.props.data;

    return (
      <Paper style={styles.block} zDepth={1} >
        <b>{data.title}</b>
        <div style={styles.menu}>
          <RaisedButton
            onTouchTap={this.handleTouchTap}
            label="Actions..."
            fullWidth={true}
          />
          <Popover
            open={this.state.open}
            anchorEl={this.state.anchorEl}
            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
            targetOrigin={{horizontal: 'left', vertical: 'top'}}
            onRequestClose={this.handleRequestClose}
          >
            <Menu value={data.type}>
              <MenuItem value={'default'} primaryText="Default"   onTouchTap={this.handleChange.bind(this, 'default')}/>
              <MenuItem value={'active'}  primaryText="Active"    onTouchTap={this.handleChange.bind(this, 'active')}/>
              <MenuItem value={'stop'}    primaryText="Stop"      onTouchTap={this.handleChange.bind(this, 'stop')}/>
              <MenuItem value={'archive'} primaryText="Archive"   onTouchTap={this.handleChange.bind(this, 'archive')}/>
              <MenuItem primaryText="Update"   leftIcon={<Update />} onTouchTap={this.handleUpdate}/>
              <MenuItem primaryText="Remove" leftIcon={<Delete />} onTouchTap={this.handleRemove}/>
            </Menu>
          </Popover>
        </div>
      </Paper>
    );
  }
}
