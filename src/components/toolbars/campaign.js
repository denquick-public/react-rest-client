import React, { Component } from 'react';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import RaisedButton from 'material-ui/RaisedButton';
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar';

const defaultState = {
  filter: 'all',
}

export default class MainToolbarPanel extends Component {

  constructor(props) {
    super(props);

    this.state = {
      filter: this.props.campaign.filter,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleCreateCampaign = this.handleCreateCampaign.bind(this);
    this.handleReloadList = this.handleReloadList.bind(this);
  }

  handleChange(event, index, value) {
    this.props.campaignActions.setupFilter(value);
    this.props.campaign.filter = value;
    this.setState({filter: value});

    this.handleReloadList();
  }

  handleReloadList() {
    const params = { userId: this.props.account._id }
    params.type = this.props.campaign.filter;

    this.props.campaignActions.read({
      params,
    }, () => {
      console.log('success');
    });
  }

  handleCreateCampaign() {
    const { open } = this.props.campaignModal;
    if (!open) {
      this.props.campaignModalActions.open();
    }
  }

  render() {

    return (
      <Toolbar>
        <ToolbarGroup firstChild={true}>
          <DropDownMenu value={this.state.filter} onChange={this.handleChange}>
            <MenuItem value={'all'} primaryText="All" />
            <MenuItem value={'active'} primaryText="Actived" />
            <MenuItem value={'stop'} primaryText="Stoped" />
            <MenuItem value={'archive'} primaryText="Archived" />
          </DropDownMenu>
        </ToolbarGroup>
        <ToolbarGroup>
          <ToolbarTitle text="" />
          <FontIcon className="muidocs-icon-custom-sort" />
          <ToolbarSeparator />
          <RaisedButton label="Reload" secondary={true} onClick={this.handleReloadList}/>
          <RaisedButton label="Create Campanign" primary={true} onClick={this.handleCreateCampaign}/>
          <IconMenu
            iconButtonElement={
              <IconButton touch={true}>
                <NavigationExpandMoreIcon />
              </IconButton>
            }
          >
            <MenuItem primaryText="Download" />
            <MenuItem primaryText="More Info" />
          </IconMenu>
        </ToolbarGroup>
      </Toolbar>
    );
  }
}
