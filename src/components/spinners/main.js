import React, { Component } from 'react';
import CircularProgress from 'material-ui/CircularProgress';
import RefreshIndicator from 'material-ui/RefreshIndicator';

const styles = {
  loading: {
    marginRight: 10,
  },
  refresh: {
    display: 'inline-block',
    position: 'relative',
  },
};

export default class Spinner extends Component {

  render() {
    const spinner = (<span style={styles.loading}>
        <RefreshIndicator
          size={28}
          left={0}
          top={0}
          status="loading"
          style={styles.refresh}
        />
      </span>);
    return this.props.show ? spinner : <span />;
  }
}
