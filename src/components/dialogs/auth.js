import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import LoginForm from '../../containers/forms/login';
import JoinForm from '../../containers/forms/join';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
};

export default class AuthDialog extends Component {
  constructor(props) {
    super(props);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose(e) {
    this.props.authModalActions.close();
  }

  handleToggle(vector) {
    const isLogin = this.props.authModal.loginTab;
    if (isLogin !== vector) {
      this.props.authModalActions.open({
        loginTab: vector,
      });
    }
  }

  render() {
    const reg = <div>reg</div>;
    let content = <LoginForm />;
    if (!this.props.authModal.loginTab) {
      content = <JoinForm />;
    }

    return (
      <div>
        <Dialog
          title={null}
          modal={false}
          open={this.props.open}
          onRequestClose={this.handleClose}
        >
          <div>
            <div>
              <FlatButton label="Login" primary={true} onClick={this.handleToggle.bind(this, true)}/>
              <FlatButton label="Join" primary={true} onClick={this.handleToggle.bind(this, false)}/>
            </div>
            {content}
          </div>
        </Dialog>
      </div>
    );
  }
}
