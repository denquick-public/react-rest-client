import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import CampaignCreateForm from '../../containers/forms/campaign-create';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
};

export default class CampaignDialog extends Component {

  constructor(props) {
    super(props);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose(e) {
    this.props.campaignModalActions.close();
  }

  render() {
    return (
      <div>
        <Dialog
          title="Create a Campaign"
          modal={false}
          open={this.props.open}
          onRequestClose={this.handleClose}
        >
          <div>
              <CampaignCreateForm />
          </div>
        </Dialog>
      </div>
    );
  }
}
