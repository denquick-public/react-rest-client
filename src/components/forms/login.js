import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Spinner from '../../components/spinners/main';
import Errors from '../../constants/errors';

const styles = {
  footer: {
    float: 'right',
  },
};

export default class LoginForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
    ['handleChangeEmail', 'handleChangePassword', 'handleSubmit'].forEach((item)=>{
      this[item] = this[item].bind(this);
    });
  }

  handleChangeEmail(event) {
    const email = event.target.value;
    this.setState({ email });
  }

  handleChangePassword(event) {
    const password = event.target.value;
    this.setState({ password });
  }

  handleSubmit() {
    this.props.accountActions.login(this.state, ()=> {
      this.props.authModalActions.close();
    });
  }

  render() {
    const { email, password } = this.state;
    const isLoading = this.props.account.loading;
    return (
      <ValidatorForm ref="form" onSubmit={this.handleSubmit} >
        <div>
          <TextValidator
            floatingLabelText="Email"
            onChange={this.handleChangeEmail}
            name="email"
            value={email}
            validators={['required', 'isEmail']}
            errorMessages={[Errors.REQUIRED, Errors.EMAIL_IS_NOT_VALID]}
          />
        </div>

        <div>
          <TextValidator
            floatingLabelText="Pasword"
            onChange={this.handleChangePassword}
            name="password"
            type="password"
            value={password}
            validators={['required']}
            errorMessages={[Errors.REQUIRED]}
          />
        </div>

        <div style={styles.footer}>

          <Spinner show={isLoading}/>

          <RaisedButton label="Submit" secondary={true} onClick={this.handleSubmit} />
        </div>

      </ValidatorForm>
    );
  }
}
