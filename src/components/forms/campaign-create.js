import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Spinner from '../../components/spinners/main';
import Errors from '../../constants/errors';

const styles = {
  footer: {
    float: 'right',
  },
};

const defaultState = {
  title: '',
};

export default class CampaignCreateForm extends Component {

  constructor(props) {
    super(props);
    this.state = defaultState;
    ['handleChange', 'handleSubmit'].forEach((item)=>{
      this[item] = this[item].bind(this);
    });
  }

  handleChange(event) {
    const title = event.target.value;
    this.setState({ title });
  }

  handleSubmit() {
    this.props.campaignActions.create(this.state, ()=> {
      this.props.campaignModalActions.close();

      const params = { userId: this.props.account._id };
      this.props.campaignActions.read({params}, ()=> {});
    });
  }

  render() {
    const { title } = this.state;
    const isLoading = false;

    return (
      <ValidatorForm ref="form" onSubmit={this.handleSubmit} >
        <div>
          <TextValidator
            floatingLabelText="Title"
            onChange={this.handleChange}
            name="title"
            value={title}
            validators={['required']}
            errorMessages={[Errors.REQUIRED]}
          />
        </div>

        <div style={styles.footer}>
          <Spinner show={isLoading}/>
          <RaisedButton label="Submit" secondary={true} onClick={this.handleSubmit} />
        </div>

      </ValidatorForm>
    );
  }
}
