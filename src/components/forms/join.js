import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Spinner from '../../components/spinners/main';
import Errors from '../../constants/errors';

const styles = {
  footer: {
    float: 'right',
  },
};

export default class JoinForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
      isValid: false,
    };

    ['handleSubmit', 'handleChange'].forEach((item) => {
      this[item] = this[item].bind(this);
    });
  }

  isValid() {
    const form = this.refs.form;
    let counts = 0;
    if (form) {
      form.childs.forEach((item)=>{
        if (!item.state.isValid) {
          counts++;
        }
      });
    }
    return counts === 0;
  }

  componentWillMount() {
    ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
      return this.state.password === this.state.confirmPassword;
    });
  }

  handleChange(event) {
     const state = this.state;
     state[event.target.name] = event.target.value;
     this.setState(state);
     this.setState({isValid: this.isValid()});
  }

  handleSubmit(event) {
    if (this.isValid()) {
      console.log('pass');
      this.props.accountActions.join(this.state, ()=> {
        this.props.authModalActions.close();
      });
    }
  }

  render() {
    const { email, password, confirmPassword } = this.state;
    const isLoading = this.props.account.loading;
    return (
      <ValidatorForm ref="form" onSubmit={this.handleSubmit} instantValidate={true} >
        <div>
          <TextValidator
            floatingLabelText="Email"
            onChange={this.handleChange}
            name="email"
            value={email}
            validators={['required', 'isEmail']}
            errorMessages={[Errors.REQUIRED, Errors.EMAIL_IS_NOT_VALID]}
          />
        </div>

        <div>
          <TextValidator
            floatingLabelText="Pasword"
            onChange={this.handleChange}
            name="password"
            type="password"
            value={password}
            validators={['required', 'isPasswordMatch']}
            errorMessages={[Errors.REQUIRED, Errors.CONFIRM_PASSWORD]}
          />
        </div>

        <div>
          <TextValidator
            floatingLabelText="Confirm Pasword"
            onChange={this.handleChange}
            name="confirmPassword"
            type="password"
            value={confirmPassword}
            validators={['required', 'isPasswordMatch']}
            errorMessages={[Errors.REQUIRED, Errors.CONFIRM_PASSWORD]}
          />
        </div>

        <div style={styles.footer}>

          <Spinner show={isLoading}/>

          <RaisedButton label="Submit" secondary={true} onClick={this.handleSubmit}/>
        </div>

      </ValidatorForm>
    );
  }
}
