import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import {Tabs, Tab} from 'material-ui/Tabs';
import builderContainer from '../../libs/container-builder';
import Spinner from '../../components/spinners/main';
import Errors from '../../constants/errors';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
};

const defaultState = {
  tab: 'a',
  campaign: {
    title: '',
    type: 'default',
    message: {
      value: '',
    }
  }
};

class CampaignEditForm extends Component {

  constructor(props) {
    super(props);
    const campaign = this.props.campaignEdit.entity;
    campaign.message = Object.assign({}, defaultState.campaign.message, campaign.message);
    this.state = Object.assign({}, defaultState, { campaign });
  }

  handleChangeTitle = (event) => {
    const title = event.target.value;
    const { campaign } = this.state;
    campaign.title = title;
    this.setState(Object.assign({}, { campaign }));
  }

  handleChangeType = (event, index, value) => {
    const type = value;
    const { campaign } = this.state;
    campaign.type = type;
    this.setState(Object.assign({}, { campaign }));
  }

  handleChangeMessageValue = (event, index, value) => {
    const { campaign } = this.state;
    campaign.message.value = event.target.value;
    this.setState(Object.assign({}, { campaign }));
  }

  handleChangeTab = (event, index, value) => {
    const tab = event;
    this.setState({ tab });
  }

  handleSubmit = () => {
    const entity = this.props.campaignEdit.entity;
    const campaignId = entity._id;
    const payload = Object.assign({ campaignId }, this.state.campaign);

    this.props.campaignActions.update(payload, ()=> {
      this.setState({ campaign: payload });
      const params  = { campaignId };
      this.props.campaignActions.read(params, ()=> {});
    });
  }

  render() {
    const { title, type } = this.state.campaign;
    const isLoading = false;
    const firstTab = (
      <div>
        <div>
          <TextValidator
            floatingLabelText="Title"
            onChange={this.handleChangeTitle}
            name="title"
            value={title}
            validators={['required']}
            errorMessages={[Errors.REQUIRED]}
          />
        </div>
        <div>
          <DropDownMenu value={type} onChange={this.handleChangeType} className={"user-menu-item"} >
            <MenuItem value={"default"} primaryText="Default" />
            <MenuItem value={"active"}  primaryText="Active" />
            <MenuItem value={"stop"}  primaryText="Stop" />
            <MenuItem value={"archive"}  primaryText="Archive" />
          </DropDownMenu>
        </div>
      </div>
    );
    const twoTab = (
      <div>
        <div>
          <TextValidator
            floatingLabelText="Message"
            onChange={this.handleChangeMessageValue}
            name="message"
            value={this.state.campaign.message.value}
          />
        </div>
      </div>
    );

    return (
      <ValidatorForm ref="form" onSubmit={this.handleSubmit} style={styles.form}>
        <Tabs value={this.state.tab} onChange={this.handleChangeTab}>
          <Tab label="General" value="a">{firstTab}</Tab>
          <Tab label="Message" value="b">{twoTab}</Tab>
          <Tab label="Design" value="c">
            <div>2222</div>
          </Tab>
        </Tabs>

        <div >
          <Spinner show={isLoading}/>
          <RaisedButton label="Submit" secondary={true} onClick={this.handleSubmit} />
        </div>
      </ValidatorForm>
    );
  }
}

export default builderContainer(CampaignEditForm);
