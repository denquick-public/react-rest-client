import React, { Component } from 'react';
import FontIcon from 'material-ui/FontIcon';
import { RaisedButton, FlatButton, AppBar, IconButton } from 'material-ui';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import AccountButton from '../containers/buttons/account-popup';
import ButtonLogout from '../containers/buttons/logout';
import AuthDialog from '../containers/dialogs/auth';
import CampaignhDialog from '../containers/dialogs/campaign';

const styles = {
  accountBlock: {
    width: 500,
  }
};

export default class App extends Component {

  render() {
    const isOpenModal = this.props.authModal.open;
    const isOpenModalCampaign = this.props.campaignModal.open;
    const isLoginTab = this.props.authModal.open;
    const buttonLogin = <AccountButton secondary={true} label="Login" loginTab={true} />
    const buttonLogout = <ButtonLogout secondary={true} />
    const accountEmail = this.props.account.email;

    return (
      <div>
        <div>
          <AuthDialog open={isOpenModal} loginTab={isLoginTab}/>
          <CampaignhDialog open={isOpenModalCampaign} />
        </div>
        <AppBar
          title="Title"
          iconElementRight={accountEmail ? buttonLogout : buttonLogin}
        />
      </div>
    );
  }
}
