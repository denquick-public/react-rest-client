import React, { Component } from 'react';
import HelloPanel from '../../containers/panels/hello';
import CampaignPanel from '../../containers/panels/campaign/main';

export default class MainPanel extends Component {

  render() {
    const { email } =  this.props.account;
    const content = email ? <CampaignPanel /> : <HelloPanel />

    return (
      content
    );
  }
}
