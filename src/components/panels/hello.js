import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import * as colors from 'material-ui/styles/colors';
import AccountButton from '../../containers/buttons/account-popup';

const styles = {
  button: {
    margin: 12,
  },
  panel: {
    fontFamily: 'Roboto, sans-serif',
    color: 'white',
    textAlign: 'center',
    paddingTop: 150,
    height: 800,
    backgroundColor: colors.indigo700
  }
};

export default class Hello extends Component {

  render() {
    const isLoggined = this.props.account.email;
    let content = '';

    if (isLoggined) {
      content = <div>
        <h2>Hello {this.props.account.email}</h2>
      </div>;
    } else {
      content = <div>
        <h2>You are not authorized</h2>
        <div>
          <AccountButton primary={true} label="Login" loginTab={true}/>
          <AccountButton secondary={true} label="Join" loginTab={false}/>
        </div>
      </div>;
    }

    return (
      <div style={styles.panel}>{content}</div>
    );
  }
}
