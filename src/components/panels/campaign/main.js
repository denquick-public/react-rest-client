import React, { Component } from 'react';
import * as colors from 'material-ui/styles/colors';
import CampaignToolbar from '../../../containers/toolbars/campaing';
import CampaignListPanel from '../../../containers/panels/campaign/list';
import CampaignEditPanel  from '../../../containers/panels/campaign/edit';

const styles = {
  button: {
    margin: 12,
  },
  panel: {
    fontFamily: 'Roboto, sans-serif',
    color: 'white',
    padding: 15,
    height: 800,
    backgroundColor: colors.indigo700
  }
};

export default class CampaignPanel extends Component {

  render() {
    let content = (<div>
      <CampaignToolbar />
      <CampaignListPanel />
    </div>);

    if ('_id' in this.props.campaignEdit.entity) {
      content = <CampaignEditPanel data={this.props.data} />
    }
    return (
      <div style={styles.panel}>
        {content}
      </div>
    );
  }
}
