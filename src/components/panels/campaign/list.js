import React, { Component } from 'react';
import * as colors from 'material-ui/styles/colors';
import Paper from 'material-ui/Paper';
import ItemBlock from '../../../containers/blocks/item';

const styles = {
  button: {
    margin: 12,
  },
  panel: {
    fontFamily: 'Roboto, sans-serif',
    color: 'white',
    padding: 15,
    height: 800,
    backgroundColor: colors.indigo700
  },
  block: {
    height: 150,
    width: 150,
    margin: 20,
    textAlign: 'center',
    display: 'inline-block',
  }
};

export default class CampaignPanelList extends Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    console.log('start mount');
    const params = { userId: this.props.account._id };

    this.props.campaignActions.read({
      params,
    }, (dispatch) => {
      console.log(1111, dispatch);
    });
  }

  render() {
    const campaignList = this.props.campaign.list || [];
    const collection = campaignList.map((item, index) => {
      return <ItemBlock key={index} data={item} />
    });
    const content = campaignList.length > 0 ? collection : <div>Not found data</div>

    return (
      <div style={styles.panel}>
        {content}
      </div>
    );
  }
}
