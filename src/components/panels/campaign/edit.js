import React, { Component } from 'react';
import * as colors from 'material-ui/styles/colors';
import RaisedButton from 'material-ui/RaisedButton';
import EditForm from '../../forms/campaign-edit';

const styles = {
  button: {
    margin: 15,
  },
  header: {
  },
  body: {
    backgroundColor: 'white',
    padding: 25,
  },
  title: {
    float: 'right',
    paddingRight: 15,
    fontSize: 24,
    fontFamily: 'Roboto, sans-serif',
    color: 'white',
    textAlign: 'center',
  }
};

export default class CampaignPanelEdit extends Component {

  handleBackToList =() => {
    this.props.campaignEditActions.toEmpty();
  }

  render() {
    const campaign = this.props.campaignEdit.entity;
    const content = (
      <div>
        <div style={styles.header}>
          <RaisedButton
            onClick={this.handleBackToList}
            label="Back to list"
            secondary={true}
            primary={false}
            style={styles.button}
          />

          <span style={styles.title}>Edit campaign a <b>{campaign.title}</b></span>
        </div>
        <div style={styles.body}>
          <EditForm />
        </div>
      </div>
  );

    return (
      <div>
        {content}
      </div>
    );
  }
}
