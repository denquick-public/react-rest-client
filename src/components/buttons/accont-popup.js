import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';

const styles = {
  button: {
    margin: 12,
  },
};

export default class AccountPopupButton extends Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    const { loginTab } = this.props;
    this.props.authModalActions.open({
      loginTab,
    });
  }

  render() {
    const { secondary, primary, label } = this.props;
    return (
      <RaisedButton
        onClick={this.handleClick}
        label={label}
        secondary={secondary}
        primary={primary}
        style={styles.button}
      />
    );
  }
}
