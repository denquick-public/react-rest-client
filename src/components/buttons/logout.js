import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';

const styles = {
  button: {
    margin: 12,
  },
};

export default class ButtonLogout extends Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    this.props.accountActions.remove();
  }

  render() {
    const { secondary, primary } =  this.props;
    return (
      <RaisedButton
        onClick={this.handleClick}
        label="Logout"
        secondary={secondary}
        primary={primary}
        style={styles.button}
      />
    );
  }
}
