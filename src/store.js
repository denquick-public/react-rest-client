import { createStore, combineReducers, applyMiddleware } from 'redux';
import account from './reducers/account';
import campaign from './reducers/campaign';
import campaignEdit from './reducers/campaign-edit';
import authModal from './reducers/auth-modal';
import campaignModal from './reducers/campaign-modal';
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';

const composeEnhancers = composeWithDevTools({
  // options like actionSanitizer, stateSanitizer
});

const reducer = combineReducers({
  account,
  campaign,
  campaignEdit,
  authModal,
  campaignModal,
});

const store = createStore(reducer, {

},composeEnhancers(
  applyMiddleware(/*createLogger,*/ thunk)
));

export default store;
