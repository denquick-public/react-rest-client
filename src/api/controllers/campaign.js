import { REQUEST, SUCCESS, READ, CREATE } from '../../constants/campaign';
import { getModel } from '../emitter';

function sendData(payload, options, type = 'create') {
  const model = getModel('campaign');
  const { dispatch, callback } = options;
  const actionModel = model[type].bind(model);

  return actionModel(payload)
    .then((responce) => {
      return responce.text();
    })
    .then((responceText)=>{
      const responce = JSON.parse(responceText);
      dispatch({
        type: 'CAMPAIGN_' + type.toUpperCase(),
        payload: responce
      });
      callback();
      return responce;
    })
    .catch((e)=>{
      console.log('Error', e);
      dispatch({ type: SUCCESS });
    });
}

export default class CampaignController {

  create(payload, options = {}) {
    return sendData.call(this, payload, options, 'create');
  }

  read(payload, options = {}) {
    if ('campaignId' in payload) {
      return sendData.call(this, payload, options, 'edit_read');
    } else {
      return sendData.call(this, payload, options, 'read');
    }
  }

  delete(payload, options = {}) {
    return sendData.call(this, payload, options, 'delete');
  }

  update(payload, options = {}) {
    return sendData.call(this, payload, options, 'update');
  }

}
