import { PUSH, REMOVE, REQUEST, SUCCESS } from '../../constants/account';
import { getModel } from '../emitter';

function sendData(payload, options, type = 'login') {
  const model = getModel('account');
  const { dispatch, callback } = options;
  const actionModel = model[type].bind(model);

  return actionModel(payload)
    .then((responce) => {
      return responce.text();
    })
    .then((responceText)=>{
      const responce = JSON.parse(responceText);
      const { email, password, _id} = responce;
      if (email && password) {
        callback();
      }
      dispatch({
        type: SUCCESS,
        payload: { email, password, _id }
      });
      return responce;
    })
    .catch((e)=>{
      console.log('Error', e);
      dispatch({ type: SUCCESS });
    });
}

export default class AccountController {

  login(payload, options = {}) {
    return sendData.call(this, payload, options, 'login');
  }

  join(payload, options = {}) {
    return sendData.call(this, payload, options, 'join');
  }

}
