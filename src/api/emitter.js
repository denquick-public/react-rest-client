import AccountModel from './models/account';
import CampaignModel from './models/campaign';
import AccountController from './controllers/account';
import CampaignController from './controllers/campaign';

const poolModels = {
  account: new AccountModel(),
  campaign: new CampaignModel(),
};

const poolControllers = {
  account: new AccountController(),
  campaign: new CampaignController(),
};

export default function emitter(type = 'controller', name) {
  if (type === 'controller') {
    return poolControllers[name];
  } else {
    return poolModels[name];
  }
};

export function getModel(name) {
  return emitter('model', name);
}
export function getController(name) {
  return emitter('controller', name);
}
