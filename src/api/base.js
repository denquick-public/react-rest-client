import qs from 'qs';
import config from '../../configs/main';
import StoreService from '../libs/store-service';
import AccountProvider from './account-provider';

const { server } = config;
const defaultOptions = { method: 'GET', body: null };

export default class Base {

  get host() {
    const { host, path } = server;
    return `${host}${path}`;
  }

  constructor() {
    this.accountProvider = new AccountProvider();
  }

  builderBody(params) {
    return qs.stringify(params);
  }

  send(path, options = defaultOptions) {
    const { body, method } = options;
    const store = StoreService.store.getState();
    const { email, password, _id } = this.accountProvider.account;
    return fetch(this.host + path, {
      method,
      body : body ? JSON.stringify(body) :  null,
      headers: {
        "Content-type": "application/json",
        "user": JSON.stringify({ email, password }),
      }
    });
  }

}
