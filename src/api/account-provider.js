import StoreService from '../libs/store-service';

export default class AccountProvider {

  get account() {
    const store = StoreService.store.getState();
    return store.account;
  }

}
