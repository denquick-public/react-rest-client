import Base from './../base';

const sendData = function(payload, path) {
  const { email, password } = payload;
  const data = {
    email, password
  };

  return fetch(this.host + path, {
    method: 'POST',
    body: this.builderBody(data),
    headers: {
      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
    }
  });
}

export default class Account extends Base {

  login(payload) {
    return sendData.call(this, payload, 'account/login');
  }

  join(payload) {
    return sendData.call(this, payload, 'account/join');
  }

}
