import Base from './../base';

const sendData = function(payload, path) {
  const { title } = payload;
  const body = { title };
  return this.send(path, {
      method: 'POST',
      body,
  });
}

const readData = function(payload, path) {
  const { params } = payload;
  const userId = this.accountProvider.account._id;
  params.userId = userId;
  return this.send(path + '/?' + this.builderBody({
    params,
  }));
}

const readDataById = function(payload, path) {
  const { campaignId } = payload;
  return this.send(path + '/' + campaignId);
}

const removeData = function(payload, path) {
  return this.send(path + '/' + payload.campaignId, {
    method: 'DELETE',
  });
}

const updateData = function(payload, path) {
  const { title, type, message } = payload;
  const body = { title, type, message };
  return this.send(path + '/' + payload.campaignId, {
    method: 'PUT',
    body,
  });
}

export default class Campaign extends Base {

  create(payload) {
    return sendData.call(this, payload, 'campaing');
  }

  read(payload) {
    return readData.call(this, payload, 'campaing');
  }

  // get by Id
  edit_read(payload) {
    return readDataById.call(this, payload, 'campaing');
  }

  delete(payload) {
    return removeData.call(this, payload, 'campaing');
  }

  update(payload) {
    return updateData.call(this, payload, 'campaing');
  }
}
