import builderContainer from '../libs/container-builder';
import Component from '../components/app';

export default builderContainer(Component);
