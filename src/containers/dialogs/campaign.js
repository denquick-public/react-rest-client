import builderContainer from '../../libs/container-builder';
import Component from '../../components/dialogs/campaign';

export default builderContainer(Component);
