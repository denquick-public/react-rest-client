import builderContainer from '../../libs/container-builder';
import Component from '../../components/dialogs/auth';

export default builderContainer(Component);
