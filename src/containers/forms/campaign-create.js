import Component from '../../components/forms/campaign-create';
import builderContainer from '../../libs/container-builder';

export default builderContainer(Component);
