import Component from '../../components/forms/login';
import builderContainer from '../../libs/container-builder';

export default builderContainer(Component);
