import Component from '../../components/forms/join';
import builderContainer from '../../libs/container-builder';

export default builderContainer(Component);
