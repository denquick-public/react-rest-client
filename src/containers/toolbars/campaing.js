import builderContainer from '../../libs/container-builder';
import Component from '../../components/toolbars/campaign';

export default builderContainer(Component);
