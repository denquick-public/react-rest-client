import Component from '../../components/buttons/logout';
import builderContainer from '../../libs/container-builder';

export default builderContainer(Component);
