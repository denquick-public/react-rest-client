import Component from '../../components/blocks/item';
import builderContainer from '../../libs/container-builder';

export default builderContainer(Component);
