import builderContainer from '../../libs/container-builder';
import Component from '../../components/panels/hello';

export default builderContainer(Component);
