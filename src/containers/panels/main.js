import builderContainer from '../../libs/container-builder';
import Component from '../../components/panels/main';

export default builderContainer(Component);
