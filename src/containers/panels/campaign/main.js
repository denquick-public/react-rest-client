import builderContainer from '../../../libs/container-builder';
import Component from '../../../components/panels/campaign/main';

export default builderContainer(Component);
