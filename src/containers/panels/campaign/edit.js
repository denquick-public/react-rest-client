import builderContainer from '../../../libs/container-builder';
import Component from '../../../components/panels/campaign/edit';

export default builderContainer(Component);
