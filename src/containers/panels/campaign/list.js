import builderContainer from '../../../libs/container-builder';
import Component from '../../../components/panels/campaign/list';

export default builderContainer(Component);
